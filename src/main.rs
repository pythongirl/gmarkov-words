use std::io::{self, BufRead};
use std::iter;
use itertools::Itertools;
use gmarkov_lib::*;
use serde::Serialize;

#[derive(Clone, Debug, Eq, PartialEq, Hash, Serialize)]
enum Word {
    Word(String),
    Punctuation(String, bool, bool),
    Space,
}

fn cut_tweet(tweet: &str) -> Vec<Word> {
    iter::once(Word::Space)
        .chain(tweet.chars().map(|c| {
            // Hashtags, mentions, and contractions should count as words, so exempt those characters
            if c.is_ascii_alphanumeric() || c == '\'' || c == '#' || c == '@' {
                Word::Word(c.to_string())
            } else if c == ' ' {
                Word::Space
            } else {
                Word::Punctuation(c.to_string(), false, false)
            }
        }))
        .chain(iter::once(Word::Space))
        .coalesce(|w1, w2| {
    
            match (w1, w2) {
                (Word::Word(s1), Word::Word(s2)) => Ok(Word::Word(format!("{}{}", s1, s2))),
                
                (Word::Punctuation(s, pre_space, _), Word::Space) => Ok(Word::Punctuation(s, pre_space, true)),
                (Word::Space, Word::Punctuation(s, _, post_space)) => Ok(Word::Punctuation(s, true, post_space)),
                (Word::Punctuation(s1, pre_space, false), Word::Punctuation(s2, false, post_space)) => Ok(Word::Punctuation(format!("{}{}", s1, s2), pre_space, post_space)),
                
                (a, b) => Err((a, b))
            }
        })
        .filter(|w| w != &Word::Space)
        .collect()
}

fn main() {

    let stdin = io::stdin();
    let handle = stdin.lock();

    let tweets = handle.lines().map(|line| {
        let line = line.unwrap();
        cut_tweet(&line)
    }).collect::<Vec<_>>();

    let mut chain = MarkovChain::with_order::<usize>(2);

    for tweet in tweets {
        chain.feed(tweet.into_iter());
    }

    let model_string = serde_json::to_string(&chain).unwrap();

    println!("{}", model_string);
}
